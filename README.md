Scraper
======
The demo of a HTML scraping application

Project set up using Vagrant
===========================

Make sure the following are installed on your local system

1. **GIT** - [download](http://git-scm.com/downloads)
2. **Virtual box (4.3.28 or higher)** - [download](https://www.virtualbox.org/wiki/Downloads)
3. **Vagrant (version 1.7.2 or higher)** - [download](http://www.vagrantup.com/downloads.html)

Once all of the above are installed, follow the next steps:

1. Edit your host file and make sure it contains the following:

    > 192.168.111.224 dev.scraper.sainsburys.co.uk


    On a linux based OS it is usually located at:
    `/etc/hosts`

2. Run the following commands:

    > git clone git@bitbucket.org:blessedsolutions/scraper-demo.git

    > cd scraper-demo

3. Now run the following commands:

    > vagrant up web

    This will take several minutes when ran for the first time

5. Return to the previous tab or window with the miyagi web vm running and type the following commands:

    > vagrant ssh web

    > cd /var/www/scraper


6. This app is built using the [Symfony 2 framework](http://symfony.com/). We will now build the project. This includes downloading the Symfony 2 vendors.

    Then run the following commands:

    > composer install


7. At this point if you visit [http://dev.scraper.sainsburys.co.uk/demo](http://dev.scraper.sainsburys.co.uk/demo)

    you should see the scraped HTML for grocery items on web page represented as JSON. If so your project was set up successfully.

    You should also be able to run the app via the command line by typing the following commands

    > cd /var/www/scraper

    > php app/console scrape:demo


    If not please double check you performed the relevant previous steps correctly or come and grab me.

8. **UNIT TESTS** In the terminal run the following commands to run the tests:

    > cd /var/www/scraper

    > ./bin/phpunit -c app/  --debug --verbose


    You can should now see the output of the tests as they run.

9. Congratulations. The environment is now set up. Go and celebrate! After celebrating I suggest you do some further reading to familairise yourself conceptually with the technologies used in this demo. Some useful links (some of which were mentioned earlier) can be found below.

    ###Virtual environments and their provisioning
    - **Vagrant** [learn more](http://www.vagrantup.com)
    - **Virtual Box** [learn more](https://www.virtualbox.org/wiki/VirtualBox)
    - **Ansible** [learn more](http://www.ansible.com/home)