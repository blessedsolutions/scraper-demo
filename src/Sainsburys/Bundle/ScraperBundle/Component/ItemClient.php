<?php

namespace Sainsburys\Bundle\ScraperBundle\Component;

use GuzzleHttp\Client;

/**
 * Class ItemClient
 * @package Sainsburys\Bundle\ScraperBundle\Component
 * A custom configuration of a Guzzle client specific to a Grocery Item
 */
class ItemClient extends Client
{
    public function __construct(array $itemClientConfigOptions)
    {
        parent::__construct($itemClientConfigOptions);
    }
}