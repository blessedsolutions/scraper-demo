<?php

namespace Sainsburys\Bundle\ScraperBundle\Component;

use GuzzleHttp\Client;
use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem;
use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItemCollection;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ScraperManager
 * @package Sainsburys\Bundle\ScraperBundle\Component
 *
 * A service for basic scraping of hmtl from a specified url
 */
class ScraperManager
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var Symfony\Component\DomCrawler\Crawler
     */
    protected $crawler;

    /**
     * @const
     * String containing url for demo
     */
    const DEMO_ITEMS_URI = 'webapp/wcs/stores/servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true';

    /**
     * @var \Sainsburys\Bundle\ScraperBundle\Entity\GroceryItemCollection
     */
    protected $groceryItemCollection;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->groceryItemCollection = new GroceryItemCollection();
    }

    /**
     * @param string $uri
     * @return \Psr\Http\Message\ResponseInterface
     *
     * Fetches a page and returns it as a ResponseInterface object
     */
    protected function fetchPage($uri)
    {
        return $this->client->get($uri);
    }

    /**
     * @param string $uri
     * @return string
     * Fetches a page and returns the page content as a string
     */
    public function fetchPageSource($uri)
    {
        $response = $this->fetchPage($uri);
        return $response->getBody()->getContents();
    }

    /**
     * @param $source
     * @param $cssSelector
     * @return Crawler
     *
     * Returns the elements in the provided source that match the css selector in $cssSelector
     */
    public function getMatchedElementsFromSource($source, $cssSelector)
    {
        $crawler = new Crawler($source);
        // apply css selector filter
        return $crawler->filter($cssSelector);
    }

    /**
     * @param $uri
     * @return GroceryItemCollection
     * Returns a GroceryItemCollection of all match grocery items in the uri provided
     */
    protected function getGroceryItems($uri)
    {
        $filter = $this->getMatchedElementsFromSource($this->fetchPageSource($uri), '.product ');
        if (iterator_count($filter) > 0) {
            // iterate over filter results
            foreach ($filter as $i => $content) {

                // create crawler instance for result
                $crawler = new Crawler($content);

                // extract the values needed
                $itemData = $this->getGroceryItemDataFromSource(trim($crawler->filter('h3 a')->attr('href')));
                $groceryItem = new GroceryItem();
                $groceryItem->setTitle(trim($crawler->filter('h3 a')->text()));
                $groceryItem->setUnitPrice(trim(preg_replace( '/[^0-9\.]/', '', $crawler->filter('p.pricePerUnit')->text() )));
                $groceryItem->setDescription($itemData['description']);
                $groceryItem->setSize($itemData['size']);

                $this->groceryItemCollection->attach($groceryItem);
            }
        }
        return $this->groceryItemCollection;
    }

    /**
     * @param $url
     * @return array
     * Return an array of data from the grocery page url
     */
    protected function getGroceryItemDataFromSource($url)
    {
        $response = $this->fetchPage($url);
        $filter = $this->getMatchedElementsFromSource($response->getBody()->getContents(), 'div.productText');
        if (iterator_count($filter) > 0) {
            // iterate over filter results
            foreach ($filter as $i => $content) {
                // create crawler instance for result
                $crawler = new Crawler($content);
                // extract the values needed
                return array(
                    'description' => trim($crawler->filter('p:first-child')->text()),
                    'size' => $response->getBody()->getSize()
                );
            }
        }
    }

    /**
     * @param $uri
     * @return array mixed
     * Fetches all grocery items from the uri provided and returns them in an array
     */
    public function getGroceryItemsAsArray($uri)
    {
        $this->getGroceryItems($uri);
        $data['results'] = $this->groceryItemCollection->toArray('kb');
        $data['total'] = $this->groceryItemCollection->getTotal();

        return $data;
    }

    /**
     * @param $uri
     * @return string
     * Fetches all grocery items from the uri provided and returns them as json
     */
    public function getGroceryItemsAsJson($uri)
    {
        return json_encode($this->getGroceryItemsAsArray($uri));
    }

    /**
     * @return string
     * Fetches all grocery items from the demo uri  and returns them in an array
     */
    public function getGroceryItemsAsJsonForDemo()
    {
        return $this->getGroceryItemsAsJson(self::DEMO_ITEMS_URI);
    }
}