<?php

namespace Sainsburys\Bundle\ScraperBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DemoController extends Controller
{
    public function indexAction()
    {
        $scraper = $this->get('sainsburys_scraper.scrape');
        $response = new Response($scraper->getGroceryItemsAsJsonForDemo());
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
