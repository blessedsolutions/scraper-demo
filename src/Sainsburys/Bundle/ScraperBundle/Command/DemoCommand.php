<?php

namespace Sainsburys\Bundle\ScraperBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DemoCommand extends ContainerAwareCommand
{
    /**
     * Specify the information about the command
     */
    protected function configure()
    {
        $this
            ->setName('scrape:demo')
            ->setDescription('A demo for scraping a webpage')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scraper = $this->getContainer()->get('sainsburys_scraper.scrape');
        $output->writeln($scraper->getGroceryItemsAsJsonForDemo());
    }
}