<?php

namespace Sainsburys\Bundle\ScraperBundle\Entity;

class GroceryItem
{
    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var int $size
     */
    protected $size;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice;

    /**
     * @var string $description
     */
    protected $description;

    /**
     * @param $title
     * Set title
     */
    public function setTitle($title)
    {
        $this->title =  $title;
    }

    /**
     * @return string
     * Gets the title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $size
     * Set size
     */
    public function setSize($size)
    {
        $this->size =  $size;
    }

    /**
     * @return int
     * Gets the size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param $unitPrice
     * Set unit price
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice =  $unitPrice;
    }

    /**
     * @return float
     * Get unit price
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param $description
     * Set description
     */
    public function setDescription($description)
    {
        $this->description =  $description;
    }

    /**
     * @return string
     * Get description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $sizeUnit
     * @return array
     * Represent selected attributes in an array
     */
    public function toArray($sizeUnit = null )
    {

        return array(
            'title' => $this->getTitle(),
            'size' => ($sizeUnit == 'kb') ? $this->getSizeInKb() : $this->getSize(),
            'unit_price' => $this->getUnitPrice(),
            'description' => $this->getDescription(),
        );
    }

    /**
     * @return string
     * Convert size into kb
     */
    public function getSizeInKb()
    {
        return round(($this->getSize() / 1024), 2). 'kb';
    }
}