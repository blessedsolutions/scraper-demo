<?php

namespace Sainsburys\Bundle\ScraperBundle\Entity;

use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem;

class GroceryItemCollection extends \SplObjectStorage
{
    /**
     * @param object $item
     * @param null $data
     * @throws \Doctrine\Instantiator\Exception\InvalidArgumentException
     * Attach a GroceryItem to the collection
     */
    public function attach($item, $data = NULL)
    {
        if (!$item instanceof GroceryItem) throw new InvalidArgumentException('Item must be of type Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem ');
        parent::attach($item, $data);
    }

    /**
     * @param object $item
     * @throws \Doctrine\Instantiator\Exception\InvalidArgumentException
     * Detach the GroceryItem from the collection
     */
    public function detach($item)
    {
        if (!$item instanceof GroceryItem) throw new InvalidArgumentException('Item must be of type Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem ');
        parent::detach($item);
    }

    /**
     * @return int
     * Sum of all the unitPrice attributes in the collection
     */
    public function getTotal()
    {
        $totalPrice = 0;
        $this->rewind();
        while($this->valid()) {
            $index = $this->key();
            $item = $this->current(); // similar to current($this)
            if ($item instanceof GroceryItem) {
                $totalPrice = $totalPrice + $item->getUnitPrice();
            }
            $this->next();
        }
        return $totalPrice;
    }

    /**
     * @param null $sizeUnit
     * @return array
     * Represent this collection as an array
     */
    public function toArray($sizeUnit = null)
    {
        $data = [];
        $this->rewind();
        while($this->valid()) {
            $item = $this->current(); // similar to current($this)
            if ($item instanceof GroceryItem) {
                $data[] = $item->toArray($sizeUnit);
            }
            $this->next();
        }
        return $data;
    }

}