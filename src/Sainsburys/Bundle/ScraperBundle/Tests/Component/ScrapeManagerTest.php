<?php

namespace Sainsburys\Bundle\ScraperBundle\Tests\Component;

use Sainsburys\Bundle\ScraperBundle\Component\ScraperManager;
use Sainsburys\Bundle\ScraperBundle\Component\ItemClient;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class ScrapeManagerTest  extends \PHPUnit_Framework_TestCase
{
    /**
     * @var string
     * Sample page source from grocery item overview page
     */
    protected $pageSource;

    /**
     * @var string
     * Sample page source from apricot item page
     */
    protected $itemApricotPageSource;

    /**
     * @var string
     * Sample page source from avacado item page
     */
    protected $itemAvacadoPageSource;

    /**
     * Prepare data to be used for each test
     */
    public function setUp()
    {
        $this->pageSource = '
        <html>
        <body>
            <div class="product ">
                <div class="productInner"><div class="productInfoWrapper">
                   <div class="productInfo"><h3><a href="http://www.sainsburys.co.uk/shop/gb/groceries/ripe---ready/sainsburys-apricot-ripe---ready-320g">Sainsburys Apricot Ripe &amp; Ready 320g<img src="http://c2.sainsburys.co.uk/wcsstore7.09.1.75/SainsburysStorefrontAssetStore/wcassets/product_images/media_7572754_M.jpg" alt=""></a></h3></div>
                 </div>
                 <div class="addToTrolleytabContainer addItemBorderTop"><div class="pricingAndTrolleyOptions"><div class="priceTab activeContainer priceTabContainer" id="addItem_149117">
                 <div class="pricing"><p class="pricePerUnit">£3.00<abbr title="per">/</abbr><abbr title="unit"><span class="pricePerUnitUnit">unit</span></abbr></p><p class="pricePerMeasure">£9.38<abbr title="per">/</abbr><abbr title="kilogram"><span class="pricePerMeasureMeasure">kg</span></abbr></p></div></div></div>
            </div></div>

            <div class="product ">
                <div class="productInner"><div class="productInfoWrapper">
                   <div class="productInfo"><h3><a href="http://www.sainsburys.co.uk/shop/gb/groceries/ripe---ready/sainsburys-avocado-xl-pinkerton-loose-300g">Sainsburys Avocado Ripe &amp; Ready XL Loose 300g<img src="http://c2.sainsburys.co.uk/wcsstore7.09.1.75/ExtendedSitesCatalogAssetStore/images/catalog/productImages/51/0000000202251/0000000202251_M.jpeg" alt=""></a></h3></div>
                 </div>
                 <div class="addToTrolleytabContainer addItemBorderTop"><div class="pricingAndTrolleyOptions"><div class="priceTab activeContainer priceTabContainer" id="addItem_14911766">
                 <div class="pricing"><p class="pricePerUnit">£6.00<abbr title="per">/</abbr><abbr title="unit"><span class="pricePerUnitUnit">unit</span></abbr></p><p class="pricePerMeasure">£18.38<abbr title="per">/</abbr><abbr title="kilogram"><span class="pricePerMeasureMeasure">kg</span></abbr></p></div></div></div>
            </div></div>
        </body>
        </html>';

        $this->itemApricotPageSource = '
            <h3 class="productDataItemHeader">Description</h3>
            <div class="productText"><p>by Sainsburys Ripe and Ready Apricots</p></div>';

        $this->itemAvacadoPageSource = '
        <h3>Description</h3>
        <div class="productText"><p>Extra Large Avocado</p></div>';

    }

    /**
     * Testing the ScraperManager::fetchPageSource()
     */
    public function testFetchPageSource()
    {
        // Create a mock guzzle and queue precise response.
        $mock = new MockHandler([$this->getPsr7ResponseOverviewPage()]);
        $itemClient = $this->createItemClientFromMockHandler($mock);

        $scraperManager = new ScraperManager($itemClient);
        $mockSource = $scraperManager->fetchPageSource('fake/url');

        $this->assertEquals($mockSource, $this->pageSource);
    }

    /**
     * Testing the ScraperManager::getMatchedElementsFromSource()
     */
    public function testGetMatchedElementsFromSource()
    {
        // Create a mock guzzle and 1 response.
        $mock = new MockHandler([new Response(200, ['X-Foo' => 'Bar'])]);

        $itemClient = $this->createItemClientFromMockHandler($mock);
        $scraperManager = new ScraperManager($itemClient);
        $matches = $scraperManager->getMatchedElementsFromSource($this->pageSource, '.product');

        $this->assertEquals(iterator_count($matches), 2);
        foreach ($matches as $i => $content) {
            $this->assertTrue($content instanceof \DOMElement);
        }
    }


    /**
     * @param MockHandler $mock
     * @return ItemClient
     * Reusable function for generating ItemClients
     */
    protected function createItemClientFromMockHandler(MockHandler $mock)
    {
        $handler = HandlerStack::create($mock);
        return new ItemClient(['handler' => $handler]);
    }


    /**
     * Testing the ScraperManager::getGroceryItemsAsArray()
     */
    public function testGetGroceryItemsAsArray()
    {
        // Create a mock guzzle and queue precise response.
        $mock = new MockHandler([
            $this->getPsr7ResponseOverviewPage(),
            $this->getPsr7ResponseItem1Page(),
            $this->getPsr7ResponseItem2Page()
        ]);
        $itemClient = $this->createItemClientFromMockHandler($mock);
        $scraperManager = new ScraperManager($itemClient);
        $data = $scraperManager->getGroceryItemsAsArray('fake/url');

        $this->assertCount(2, $data);
        $this->assertCount(2, $data['results']);
        $this->assertEquals(9, $data['total']);

        //test item 1
        $this->assertEquals('Sainsburys Apricot Ripe & Ready 320g', $data['results'][0]['title']);
        $this->assertEquals(3.00, $data['results'][0]['unit_price']);
        $this->assertEquals('0kb', $data['results'][0]['size']);
        $this->assertEquals('by Sainsburys Ripe and Ready Apricots', $data['results'][0]['description']);

        //test item2
        $this->assertEquals('Sainsburys Avocado Ripe & Ready XL Loose 300g', $data['results'][1]['title']);
        $this->assertEquals(6.00, $data['results'][1]['unit_price']);
        $this->assertEquals('0kb', $data['results'][1]['size']);
        $this->assertEquals('Extra Large Avocado', $data['results'][1]['description']);
    }

    /**
     * Testing the ScraperManager::testGetGroceryItemsAsJson()
     */
    public function testGetGroceryItemsAsJson()
    {
        // Create a mock guzzle and queue precise response.
        $mock = new MockHandler([
            $this->getPsr7ResponseOverviewPage(),
            $this->getPsr7ResponseItem1Page(),
            $this->getPsr7ResponseItem2Page()
        ]);
        $itemClient = $this->createItemClientFromMockHandler($mock);
        $scraperManager = new ScraperManager($itemClient);
        $json = $scraperManager->getGroceryItemsAsJson('fake/url');

        $this->assertTrue(count($json) > 0);
        $this->assertTrue(is_object(json_decode($json)));
        $data = json_decode($json, true);

        $this->assertEquals(9, $data['total']);

        //test item1
        $this->assertEquals('Sainsburys Apricot Ripe & Ready 320g', $data['results'][0]['title']);
        $this->assertEquals(3.00, $data['results'][0]['unit_price']);
        $this->assertEquals('0kb', $data['results'][0]['size']);
        $this->assertEquals('by Sainsburys Ripe and Ready Apricots', $data['results'][0]['description']);

        //test item2
        $this->assertEquals('Sainsburys Avocado Ripe & Ready XL Loose 300g', $data['results'][1]['title']);
        $this->assertEquals(6.00, $data['results'][1]['unit_price']);
        $this->assertEquals('0kb', $data['results'][1]['size']);
        $this->assertEquals('Extra Large Avocado', $data['results'][1]['description']);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Stream for grocery item overview page
     */
    protected function getPsr7StreamForOverviewPage()
    {
        $psr7StreamOverviewPage = $this->getMockBuilder('GuzzleHttp\Psr7\Stream')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7StreamOverviewPage->method('getContents')
            ->willReturn($this->pageSource);

        return $psr7StreamOverviewPage;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Response for grocery item overview page
     */
    protected function getPsr7ResponseOverviewPage()
    {
        $psr7ResponseOverviewPage = $this->getMockBuilder('GuzzleHttp\Psr7\Response')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7ResponseOverviewPage->method('getBody')
            ->willReturn($this->getPsr7StreamForOverviewPage());

        return $psr7ResponseOverviewPage;
    }


    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Stream for grocery item1's page
     */
    protected function getPsr7StreamItem1Page()
    {
        $psr7StreamItem1Page = $this->getMockBuilder('GuzzleHttp\Psr7\Stream')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7StreamItem1Page->method('getContents')
            ->willReturn($this->itemApricotPageSource);

        return $psr7StreamItem1Page;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Response for grocery item1's page
     */
    protected function getPsr7ResponseItem1Page()
    {
        $psr7ResponseItem1Page = $this->getMockBuilder('GuzzleHttp\Psr7\Response')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7ResponseItem1Page->method('getBody')
            ->willReturn($this->getPsr7StreamItem1Page());

        return $psr7ResponseItem1Page;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Stream for grocery item2's page
     */
    protected function getPsr7StreamItem2Page()
    {
        $psr7StreamItem2Page = $this->getMockBuilder('GuzzleHttp\Psr7\Stream')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7StreamItem2Page->method('getContents')
            ->willReturn($this->itemAvacadoPageSource);

        return $psr7StreamItem2Page;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     * Reusable function for generating GuzzleHttp\Psr7\Response for grocery item2's page
     */
    protected function getPsr7ResponseItem2Page ()
    {
        $psr7ResponseItem2Page = $this->getMockBuilder('GuzzleHttp\Psr7\Response')
            ->disableOriginalConstructor()
            ->getMock();
        $psr7ResponseItem2Page->method('getBody')
            ->willReturn($this->getPsr7StreamItem2Page());

        return $psr7ResponseItem2Page;
    }
} 