<?php

namespace Sainsburys\Bundle\ScraperBundle\Tests\Entity;

use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem;
use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItemCollection;
use Doctrine\Instantiator\Exception\InvalidArgumentException;

class GroceryItemCollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var GroceryItem $item
     */
    protected $item;

    /**
     * @var GroceryItem $item2
     */
    protected $item2;

    /**
     * Set values before each test
     */
    public function setUp()
    {
        $this->item =  new GroceryItem();
        $this->item->setTitle('my title');
        $this->item->setSize(1024);
        $this->item->setUnitPrice(10.00);
        $this->item->setDescription('my desc');

        $this->item2 =  new GroceryItem();
        $this->item2->setTitle('my title2');
        $this->item2->setSize(2048);
        $this->item2->setUnitPrice(20.00);
        $this->item2->setDescription('my desc2');
    }

    /**
     * Testing GroceryItemCollection::attach()
     */
    public function testAttach()
    {
        $collection = new GroceryItemCollection();
        $collection->attach($this->item);
        $collection->attach($this->item2);
        $this->assertEquals(2, $collection->count());

        $fakeObj = new \stdClass();
        try {
            $collection->attach($fakeObj);
            $this->fail('InvalidArgumentException should have been thrown.');
        } catch (InvalidArgumentException $e) {}

    }


    /**
     * Testing GroceryItemCollection::detach()
     */
    public function testDetach()
    {
        $collection = new GroceryItemCollection();
        $collection->attach($this->item);
        $collection->attach($this->item2);

        $collection->detach($this->item);
        $this->assertEquals(1, $collection->count());

        $collection->detach($this->item2);
        $this->assertEquals(0, $collection->count());

        $fakeObj = new \stdClass();
        try {
            $collection->detach($fakeObj);
            $this->fail('InvalidArgumentException should have been thrown.');
        } catch (InvalidArgumentException $e) {}

    }

    /**
     * Testing GroceryItemCollection::getTotal()
     */
    public function testGetTotal()
    {
        $collection = new GroceryItemCollection();
        $collection->attach($this->item);
        $collection->attach($this->item2);

        $this->assertEquals(30.00, $collection->getTotal());
        $collection->detach($this->item);
        $this->assertEquals(20.00, $collection->getTotal());
        $collection->detach($this->item2);
        $this->assertEquals(0, $collection->getTotal());
    }

    /**
     * Testing GroceryItemCollection::toArray()
     */
    public function testToArray()
    {
        $collection = new GroceryItemCollection();
        $collection->attach($this->item);
        $collection->attach($this->item2);

        $data = $collection->toArray();
        $this->assertEquals(2, count($data));
        $this->assertTrue(is_array($data[0]));
        $this->assertTrue(is_array($data[1]));
    }
}