<?php

namespace Sainsburys\Bundle\ScraperBundle\Tests\Entity;

use Sainsburys\Bundle\ScraperBundle\Entity\GroceryItem;

class GroceryItemTest  extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing GroceryItem::toArray()
     */
    public function testToArray()
    {
        $item =  new GroceryItem();
        $item->setTitle('my title');
        $item->setSize(1024);
        $item->setUnitPrice(10.00);
        $item->setDescription('my desc');

        $data = $item->toArray();

        $this->assertEquals($data['title'], $item->getTitle());
        $this->assertEquals($data['size'], $item->getSize());
        $this->assertEquals($data['unit_price'], $item->getUnitPrice());
        $this->assertEquals($data['description'], $item->getDescription());

        $data2 = $item->toArray('kb');
        $this->assertEquals($data2['size'], $item->getSizeInKb());
    }

    /**
     * Testing GroceryItem::GroceryItem()
     */
    public function testGetSizeInKb()
    {
        $item =  new GroceryItem();
        $item->setSize(1024);
        $this->assertEquals('1kb', $item->getSizeInKb());
    }

}
