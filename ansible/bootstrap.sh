#!/bin/bash

# Install Ansible in the VM
#if [ ! -f /usr/bin/ansible ]; then
    rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
    yum install python-setuptools -y
    yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel -y
    yum install ansible -y
#fi

yum update -y --exclude=kernel*

sudo chkconfig iptables off

# Kickstart provisioning with Ansible in local

cd /vagrant
ansible-galaxy install --role-file=ansible/galaxy-roles.txt --ignore-errors --force
ansible-playbook -i ansible/hosts ansible/playbook-webserver-local.yml -vvvv

